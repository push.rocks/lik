import { tap, expect } from '@pushrocks/tapbundle';

import { LimitedArray } from '../ts/index.js';

let testLimitedArray: LimitedArray<string>;

tap.test('should create a LimitedArray', async () => {
  testLimitedArray = new LimitedArray(6);
  expect(testLimitedArray).toBeInstanceOf(LimitedArray);
});

tap.test('should never be longer than the set length', async () => {
  testLimitedArray.addMany(['hi', 'this', 'is', 'quite', 'a', 'long', 'string', ':)']);
  expect(testLimitedArray.array.length < 7).toBeTrue();
});

tap.start();
