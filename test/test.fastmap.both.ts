import { tap, expect } from '@pushrocks/tapbundle';

import * as lik from '../ts/index.js';

tap.test('should create a valid fastmap', async () => {
  const fastmap = new lik.FastMap();
  expect(fastmap).toBeInstanceOf(lik.FastMap);
});

tap.test('should find an entry', async () => {
  const fastmap = new lik.FastMap<{
    value1: string;
    value2: string;
  }>();
  fastmap.addToMap('heythere', {
    value1: 'heyho',
    value2: 'heyho2',
  });
  fastmap.addToMap('heythere2', {
    value1: 'heyho3',
    value2: 'heyho4',
  });

  const result = await fastmap.find(async (itemArg) => {
    return itemArg.value2 === 'heyho4';
  });
  expect(result.value1).toEqual('heyho3');
});

tap.start();
